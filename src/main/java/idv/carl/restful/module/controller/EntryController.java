package idv.carl.restful.module.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.stereotype.Controller;

@Controller
@Path("/rest-module")
public class EntryController {
	
	@GET
	@Path("/greeting")
	@Produces("application/json")
	public String greeting() {
		return "I said yeah~";
	}

}

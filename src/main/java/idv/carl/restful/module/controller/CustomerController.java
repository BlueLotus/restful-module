package idv.carl.restful.module.controller;

import java.net.URI;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import idv.carl.restful.module.domain.Customer;
import idv.carl.restful.module.service.CustomerService;

@Path("/customer-service")
@Component
@Produces(MediaType.APPLICATION_JSON)
public class CustomerController {
	
	@Context
	private UriInfo uriInfo;
	
	private CustomerService customerService;
	
	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@POST
	@Path("/customer")
	public Response createCustomer(Customer customer) {
		customer = customerService.save(customer);
		URI location = uriInfo.getAbsolutePathBuilder()
						.path("{id}")
						.resolveTemplate("id", customer.getId())
						.build();
		return Response.created(location).build();
	}
	
	@GET
	@Path("/customers/all")
	public Iterable<Customer> findAll() {
		Iterable<Customer> customers = customerService.findAll();
		return customers;
	}
	
	@GET
	@Path("/customers")
	public Iterable<Customer> findAllWithPagination(
					@QueryParam("page") @DefaultValue("0") int page,
					@QueryParam("size") @DefaultValue("20") int size,
					@QueryParam("sort") @DefaultValue("lastname") List<String> sort,
					@QueryParam("direction") @DefaultValue("asc") String direction) {
		return customerService.findAll(new PageRequest(
				page, 
				size, 
				Sort.Direction.fromString(direction), 
				sort.toArray(new String[0])));
	}
	
	@GET
	@Path("/customer/{id}")
	public Customer findById(@PathParam("id") Long id) {
		return customerService.findById(id);
	}
	
	@POST
	@Path("/customer/{id}")
	public Response updateCustomer(@PathParam("id") Long id, Customer customer) {
		verifyCustomer(id);
		customer.setId(id);
		customerService.save(customer);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/customer/{id}")
	public Response deleteCustomer(@PathParam("id") Long id) {
		verifyCustomer(id);
		customerService.delete(id);
		return Response.accepted().build();
	}
	
	private void verifyCustomer(Long id) {
		Customer customer = customerService.findById(id);
		if(customer == null) {
			throw new RuntimeException("Customer not found.");
		}
	}

}

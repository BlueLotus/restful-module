package idv.carl.restful.module.configuration;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import idv.carl.restful.module.controller.CustomerController;
import idv.carl.restful.module.controller.EntryController;

@Configuration
public class JerseyConfig extends ResourceConfig {
	
	public JerseyConfig() {
		register(EntryController.class);
		register(CustomerController.class);
	}

}

package idv.carl.restful.module.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import idv.carl.restful.module.domain.Customer;
import idv.carl.restful.module.domain.EmailAddress;
import idv.carl.restful.module.repository.CustomerRepository;
import idv.carl.restful.module.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	CustomerRepository customerRepository;
	
	@Autowired
	public CustomerServiceImpl(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public Customer save(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Iterable<Customer> findAll() {
		return customerRepository.findAll();
	}
	
	@Override
	public Iterable<Customer> findAll(PageRequest pageRequest) {
		return customerRepository.findAll(pageRequest);
	}

	@Override
	public Customer findById(Long id) {
		return customerRepository.findOne(id);
	}

	@Override
	public Customer findByEmail(EmailAddress email) {
		return customerRepository.findByEmailAddress(email.getValue());
	}

	@Override
	public void delete(Long id) {
		customerRepository.delete(id);
	}

}

package idv.carl.restful.module.service;

import org.springframework.data.domain.PageRequest;

import idv.carl.restful.module.domain.Customer;
import idv.carl.restful.module.domain.EmailAddress;

public interface CustomerService {
	
	public Customer save(Customer customer);
	
	public Iterable<Customer> findAll();
	
	public Iterable<Customer> findAll(PageRequest pageRequest);
	
	public Customer findById(Long id);
	
	public Customer findByEmail(EmailAddress email);
	
	public void delete(Long id);

}

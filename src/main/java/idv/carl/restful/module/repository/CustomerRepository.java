package idv.carl.restful.module.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import idv.carl.restful.module.domain.Customer;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
	
	public Customer findByEmailAddress(String emailAddress);

}

package idv.carl.restful.module.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.util.Assert;

@Entity
public class Customer extends AbstractEntity {
	
	@Column
	private String firstname;
	
	@Column
	private String lastname;
	
	@Column
	private EmailAddress emailAddress;
	
	public Customer(String firstname, String lastname) {
		Assert.hasText(firstname);
		Assert.hasText(lastname);
		
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	protected Customer() {
		
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public EmailAddress getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(EmailAddress emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String toString() {
		return "Customer [firstname=" + firstname + ", lastname=" + lastname + ", emailAddress=" + emailAddress.getValue()
				+ ", getId()=" + getId() + "]";
	}

}

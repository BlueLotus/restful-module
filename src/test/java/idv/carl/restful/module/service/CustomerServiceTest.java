package idv.carl.restful.module.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import idv.carl.restful.module.domain.Customer;
import idv.carl.restful.module.domain.EmailAddress;
import idv.carl.restful.module.repository.CustomerRepository;
import idv.carl.restful.module.service.impl.CustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerServiceTest {
	
	@Mock
	private CustomerRepository customerRepository;
	
	@InjectMocks
	private CustomerServiceImpl customerService;
	
	@Test
	public void test1SaveCustomer() {
		Customer customer = new Customer("RuRu", "Cheng");
		Customer cretated = new Customer("RuRu", "Cheng");
		cretated.setId(2l);
		when(customerRepository.save(customer)).thenReturn(cretated);
		Customer result = customerService.save(customer);
		verify(customerRepository, times(1)).save(customer);
		assertEquals(2l, result.getId().longValue());
	}
	
	@Test
	public void test2FindAllCustomers() {
		Customer customer = getCustomer(1l, "Carl", "Lu");
		List<Customer> expected = new LinkedList<Customer>();
		expected.add(customer);
		when(customerRepository.findAll()).thenReturn(expected);
		List<Customer> results = (List<Customer>) customerService.findAll();
		verify(customerRepository, times(1)).findAll();
		assertEquals(1, results.size());
	}
	
	@Test
	public void test3FindById() {
		Long id = 1l;
		Customer expected = getCustomer(id, "Carl", "Lu");
		when(customerRepository.findOne(id)).thenReturn(expected);
		Customer customer = customerService.findById(id);
		verify(customerRepository, times(1)).findOne(id);
		assertEquals(customer.getFirstname(), "Carl");
	}
	
	@Test
	public void test4FindByEmail() {
		Long id = 1l;
		String email = "Carl@gmail.com";
		Customer expected = getCustomer(id, "Carl", "Lu");
		when(customerRepository.findByEmailAddress(email)).thenReturn(expected);
		Customer customer = customerService.findByEmail(new EmailAddress(email));
		verify(customerRepository, times(1)).findByEmailAddress(email);
		assertEquals(customer.getEmailAddress().getValue(), email);
	}
	
	@Test
	public void test5UpdateCustomer() {
		Long id = 1l;
		Customer origin = getCustomer(id, "Carl", "Lu");
		origin.setFirstname("MoMo");
		Customer updated = getCustomer(id, "MoMo", "Lu");
		when(customerRepository.save(origin)).thenReturn(updated);
		when(customerRepository.findOne(id)).thenReturn(updated);
		customerService.save(origin);
		Customer result = customerService.findById(id);
		verify(customerRepository, times(1)).save(origin);
		verify(customerRepository, times(1)).findOne(id);
		assertEquals(result.getFirstname(), updated.getFirstname());
	}
	
	@Test
	public void test6DeleteCustomer() {
		Long id = 1l;
		when(customerRepository.findOne(id)).thenReturn(null);
		customerService.delete(id);
		Customer result = customerService.findById(id);
		verify(customerRepository, times(1)).findOne(id);
		assertNull(result);
	}
	
	private Customer getCustomer(Long id, String firstname, String lastname) {
		Customer expected = new Customer("Carl", "Lu");
		expected.setId(id);
		expected.setEmailAddress(new EmailAddress(firstname + "@gmail.com"));
		return expected;
	}

}

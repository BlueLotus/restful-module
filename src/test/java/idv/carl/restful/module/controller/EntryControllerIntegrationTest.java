package idv.carl.restful.module.controller;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import idv.carl.restful.module.RestfulModuleApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RestfulModuleApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port=9000")
public class EntryControllerIntegrationTest {
	
	private RestTemplate restTemplate = new RestTemplate();
	
	@Test
	public void testForGreeting() {
		ResponseEntity<String> result = restTemplate.getForEntity("http://localhost:9000/rest-module/greeting", String.class);
		assertThat(result.getStatusCode().is2xxSuccessful(), is(true));
		assertThat(result.getBody(), is("I said yeah~"));
	}
	

}

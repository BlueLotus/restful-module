package idv.carl.restful.module.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import idv.carl.restful.module.RestfulModuleApplication;
import idv.carl.restful.module.domain.Customer;
import idv.carl.restful.module.domain.EmailAddress;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RestfulModuleApplication.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerRepositoryTest {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Test
	public void test1GetCustomers() {
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		assertEquals(1, customers.size());
		Customer customer = customers.get(0);
		assertEquals("yotsuba1022@gmail.com", customer.getEmailAddress().getValue());
		assertEquals("Carl", customer.getFirstname());
		assertEquals("Lu", customer.getLastname());
	}
	
	@Test
	public void test2GetCustomerById() {
		Customer customer = customerRepository.findOne(1l);
		assertEquals("yotsuba1022@gmail.com", customer.getEmailAddress().getValue());
		assertEquals("Carl", customer.getFirstname());
		assertEquals("Lu", customer.getLastname());
	}
	
	@Test
	public void test3AddCustomer() {
		Customer customer = new Customer("RuRu", "Cheng");
		customerRepository.save(customer);
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		assertEquals(2, customers.size());
		assertEquals("Cheng", customerRepository.findOne(2l).getLastname());
	}
	
	@Test
	public void test4UpdateCustomer() {
		Customer customer = customerRepository.findOne(2l);
		customer.setLastname("MaoMao");
		customer.setEmailAddress(new EmailAddress("ruru0513@gmail.com"));
		customerRepository.save(customer);
		customer = customerRepository.findOne(2l);
		assertEquals("ruru0513@gmail.com", customer.getEmailAddress().getValue());
		assertEquals("MaoMao", customer.getLastname());
		assertEquals("RuRu", customer.getFirstname());
	}
	
	@Test
	public void test5DeleteCustomer() {
		customerRepository.delete(2l);
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		assertEquals(1, customers.size());
		assertEquals("Carl", customerRepository.findOne(1l).getFirstname());
		assertEquals(null, customerRepository.findOne(2l));
	}
	
}

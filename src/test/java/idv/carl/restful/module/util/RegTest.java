package idv.carl.restful.module.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class RegTest {

	private static final String MOBILE_PHONE_REX = "[0-9]{4}-?[0-9]{6}";
	private static final String FIXED_PHONE_REX = "[0]{1}[0-9]{1}-?[0-9]{7,8}";
	private static final String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	@Test
	public void testForValidateMobilePhoneNumber() {
		String mobileNum = "0978-527339";
		String mobileNumTrim = "0978527339";
		String homeNum = "03-5962269";
		String homeNumTrim = "035962269";
		String homeNum2 = "02-29039905";
		
		assertTrue(mobileNum.matches(MOBILE_PHONE_REX));
		assertTrue(mobileNumTrim.matches(MOBILE_PHONE_REX));
		assertFalse(homeNum.matches(MOBILE_PHONE_REX));
		assertTrue(homeNum.matches(FIXED_PHONE_REX));
		assertTrue(homeNumTrim.matches(FIXED_PHONE_REX));
		assertTrue(homeNum2.matches(FIXED_PHONE_REX));
	}
	
	@Test
	public void testForEmailaddress() {
		String validEmail1 = "yotsuba1022@gmail.com";
		String validEmail2 = "carl.lu@rga.com";
		String validEmail3 = "carl_lu@rga.com";
		String validEmail4 = "carl.lu@qq.cn";
		String validEmail5 = "carl.lu@yahoo.tw";
		String invalidEmail = "carl.lu@gmail.c";
		assertTrue(validEmail1.matches(EMAIL_REGEX));
		assertTrue(validEmail2.matches(EMAIL_REGEX));
		assertTrue(validEmail3.matches(EMAIL_REGEX));
		assertTrue(validEmail4.matches(EMAIL_REGEX));
		assertTrue(validEmail5.matches(EMAIL_REGEX));
		assertFalse(invalidEmail.matches(EMAIL_REGEX));
	}
	
}
